package imageprocessing;

import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayU8;
import boofcv.struct.image.Planar;
import boofcv.alg.color.ColorHsv;
import java.awt.image.BufferedImage;

public class ColorProcessing {

    public static void brighten(Planar<GrayU8> input, int delta) {
        for (GrayU8 band : input.getBands()) {
            GrayLevelProcessing.brighten(band, delta);
        }
    }

    public static void meanFilter(Planar<GrayU8> input, Planar<GrayU8> output, int size) {
        for (int bandIdx : new int[] { 0, 1, 2 }) {
            Convolution.meanFilter(input.getBand(bandIdx), output.getBand(bandIdx), size);
        }
    }

    public static void shadeGray(Planar<GrayU8> input) {
        for (int x = 0; x < input.width; x++) {
            for (int y = 0; y < input.height; y++) {
                int v = 0;
                v += input.getBand(0).get(x, y) * 0.3;
                v += input.getBand(1).get(x, y) * 0.59;
                v += input.getBand(2).get(x, y) * 0.11;
                for (GrayU8 band : input.getBands()) {
                    band.set(x, y, v);
                }
            }
        }
    }

    public static void tint(Planar<GrayU8> input, int tint) {
        if (tint < 0 || tint >= 360) {
            throw new IllegalArgumentException("tint must be between 0 and 359");
        }
        input.forEachXY((int x, int y) -> {
            int r = input.getBand(0).get(x, y);
            int g = input.getBand(1).get(x, y);
            int b = input.getBand(2).get(x, y);
            double[] hsv = new double[3];
            double[] rgb = new double[3];
            ColorHsv.rgbToHsv(r / 255., g / 255., b / 255., hsv);
            ColorHsv.hsvToRgb(Math.toRadians(tint), hsv[1], hsv[2], rgb);
            input.getBand(0).set(x, y, (int) (rgb[0] * 255));
            input.getBand(1).set(x, y, (int) (rgb[1] * 255));
            input.getBand(2).set(x, y, (int) (rgb[2] * 255));
        });

    }

    public static void shadeGrayHSV(Planar<GrayU8> input) {
        input.forEachXY((int x, int y) -> {
            int r = input.getBand(0).get(x, y);
            int g = input.getBand(1).get(x, y);
            int b = input.getBand(2).get(x, y);
            double[] hsv = new double[3];
            double[] rgb = new double[3];
            ColorHsv.rgbToHsv(r / 255., g / 255., b / 255., hsv);
            ColorHsv.hsvToRgb(hsv[0], 0, hsv[2], rgb);
            input.getBand(0).set(x, y, (int) (rgb[0] * 255));
            input.getBand(1).set(x, y, (int) (rgb[1] * 255));
            input.getBand(2).set(x, y, (int) (rgb[2] * 255));
        });

    }

    public static void hueHistogram(Planar<GrayU8> input, GrayU8 output) {
        int[] hue = new int[360];
        int max = 1; // Not zero to avoid division by zero
        input.forEachXY((int x, int y) -> {
            int r = input.getBand(0).get(x, y);
            int g = input.getBand(1).get(x, y);
            int b = input.getBand(2).get(x, y);
            double[] hsv = new double[3];
            ColorHsv.rgbToHsv(r / 255., g / 255., b / 255., hsv);
            int hueAngle = (int) Math.floor(Math.toDegrees(hsv[0]));
            hue[hueAngle]++;
        });
        for (int i = 0; i < hue.length; i++) {
            max = Math.max(max, hue[i]);
        }
        for (int x = 0; x < hue.length; x++) {
            for (int y = 0; y < (hue[x] * 200) / max; y++) {
                output.set(x, 199 - y, 255);
            }
        }
    }

    public static void hueSaturationHistogram(Planar<GrayU8> input, GrayU8 output) {
        int[][] hueSat = new int[360][101];
        int max = 1; // Not zero to avoid division by zero
        input.forEachXY((int x, int y) -> {
            int[] rgb = { input.getBand(0).get(x, y), input.getBand(1).get(x, y), input.getBand(2).get(x, y) };
            double[] hsv = new double[3];
            ColorHsv.rgbToHsv(rgb[0] / 255., rgb[1] / 255., rgb[2] / 255., hsv);
            int hueAngle = (int) Math.round(Math.toDegrees(hsv[0])) % 360;
            int saturation = (int) Math.round(hsv[1] * 100);
            hueSat[hueAngle][saturation]++;
        });
        for (int x = 0; x < hueSat.length; x++) {
            for (int y = 0; y < hueSat[x].length; y++) {
                if (hueSat[x][y] > 1000)
                    max = Math.max(max, hueSat[x][y]);
            }
        }
        final int fMax = max;
        System.out.println(max);
        output.forEachXY((int x, int y) -> {
            if ((hueSat[x][y]) > 100)
                output.set(x, y, Math.min(255, hueSat[x][y]));
        });
    }

    public static void main(String[] args) {

        // load image
        if (args.length < 2) {
            System.out.println("missing input or output image filename");
            System.exit(-1);
        }
        final String inputPath = args[0];
        BufferedImage input = UtilImageIO.loadImage(inputPath);
        if (input == null) {
            System.err.println("Cannot read input file '" + inputPath);
            System.exit(-1);
        }
        Planar<GrayU8> image = ConvertBufferedImage.convertFromPlanar(input, null, true, GrayU8.class);

        // processing
        // brighten(image, 50);
        // Planar<GrayU8> output = image.createSameShape();
        // meanFilter(image, output, 20);
        // shadeGray(image);
        // tint(image, 270);
        // shadeGrayHSV(image);
        // GrayU8 output = new GrayU8(360, 200);
        // hueHistogram(image, output);
        GrayU8 output = new GrayU8(360, 101);
        hueSaturationHistogram(image, output);

        // save output image
        final String outputPath = args[1];
        // UtilImageIO.saveImage(image, outputPath);
        UtilImageIO.saveImage(output, outputPath);
        System.out.println("Image saved in: " + outputPath);
    }
}
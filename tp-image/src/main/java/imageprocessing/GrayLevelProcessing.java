package imageprocessing;

import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayU8;
import imageprocessing.Convolution;

public class GrayLevelProcessing {

	public static void threshold(GrayU8 input, int t) {
		for (int y = 0; y < input.height; ++y) {
			for (int x = 0; x < input.width; ++x) {
				int gl = input.get(x, y);
				if (gl < t) {
					gl = 0;
				} else {
					gl = 255;
				}
				input.set(x, y, gl);
			}
		}
	}

	public static void brighten(GrayU8 input, int delta) {
		input.forEachPixel((int x, int y, int gl) -> input.set(x, y, Math.max(0, Math.min(255, gl + delta))));
	}

	public static void dynamicContrast(GrayU8 input) {
		int minGl = 255;
		int maxGl = 0;
		for (int y = 0; y < input.height; y++) {
			for (int x = 0; x < input.width; x++) {
				int gl = input.get(x, y);
				minGl = Math.min(minGl, gl);
				maxGl = Math.max(maxGl, gl);
			}
		}
		final int maxGlF = maxGl;
		final int minGlF = minGl;
		input.forEachPixel((int x, int y, int gl) -> input.set(x, y, (gl - minGlF) * 255 / (maxGlF - minGlF)));
	}

	public static void dynamicContrastLut(GrayU8 input) {
		int[] lut = new int[256];
		int minGl = 255;
		int maxGl = 0;
		for (int y = 0; y < input.height; y++) {
			for (int x = 0; x < input.width; x++) {
				int gl = input.get(x, y);
				minGl = Math.min(minGl, gl);
				maxGl = Math.max(maxGl, gl);
			}
		}
		for (int i = 0; i < 255; i++) {
			lut[i] = (i - minGl) * 255 / (maxGl - minGl);
		}
		input.forEachPixel((int x, int y, int gl) -> input.set(x, y, lut[gl]));
	}

	public static void histogramEqualization(GrayU8 input) {
		int[] lut = new int[256];
		input.forEachPixel((int x, int y, int gl) -> lut[gl]++);

		int maxVal = 0;
		for (int i = 0; i < 256; i++) {
			if (i > 0) {
				lut[i] += lut[i - 1];
			}
			maxVal = Math.max(maxVal, lut[i]);
		}

		final int maxValF = maxVal;
		input.forEachPixel((int x, int y, int gl) -> input.set(x, y, (lut[gl] * 255) / maxValF));
	}

	public static void main(String[] args) {

		// load image
		if (args.length < 2) {
			System.out.println("missing input or output image filename");
			System.exit(-1);
		}
		final String inputPath = args[0];
		GrayU8 input = UtilImageIO.loadImage(inputPath, GrayU8.class);
		if (input == null) {
			System.err.println("Cannot read input file '" + inputPath);
			System.exit(-1);
		}

		// processing

		// threshold(input, 128);
		// brighten(input, -50);
		// dynamicContrast(input);
		histogramEqualization(input);

		// save output image
		final String outputPath = args[1];
		UtilImageIO.saveImage(input, outputPath);
		System.out.println("Image saved in: " + outputPath);
	}

}
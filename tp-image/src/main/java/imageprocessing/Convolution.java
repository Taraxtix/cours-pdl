package imageprocessing;

import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayS16;
import boofcv.struct.image.GrayU8;
import boofcv.core.image.ConvertImage;

public class Convolution {

  public static void meanFilter(GrayU8 input, GrayU8 output, int size) {
    input.forEachXY((int x, int y) -> {
      if (x < size || x >= input.width - size || y < size || y >= input.height - size) {
        return;
      }
      int[] neighbors = new int[(size * size) + 1];
      int maxD = size / 2;
      for (int dy = -maxD; dy <= maxD; dy++) {
        for (int dx = -maxD; dx <= maxD; dx++) {
          neighbors[(dy + (maxD)) * (maxD) + (dx + (maxD))] = input.get(x + dx, y + dy);
        }
      }
      int sum = 0;
      for (int i = 0; i < neighbors.length; i++) {
        sum += neighbors[i];
      }
      output.set(x, y, sum / neighbors.length);
    });
  }

  public static void convolution(GrayU8 input, GrayS16 output, int[][] kernel) {
    int nu = kernel.length / 2;
    int nv = kernel[0].length / 2;
    output.forEachXY((int x, int y) -> {
      int r = 0;
      for (int u = -nu; u <= nu; u++) {
        for (int v = -nv; v <= nv; v++) {
          if (x + u < 0 || x + u >= input.width || y + v < 0 || y + v >= input.height) {
            continue;
          }
          r += input.get(x + u, y + v) * kernel[u + nu][v + nv];
        }
      }
      output.set(x, y, r);
    });
  }

  public static void gradientImage(GrayU8 input, GrayU8 output, int[][] kernelX, int[][] kernelY) {
    GrayS16 gx = new GrayS16(input.width, input.height);
    GrayS16 gy = new GrayS16(input.width, input.height);
    convolution(input, gx, kernelX);
    convolution(input, gy, kernelY);

    output.forEachXY(
        (int x, int y) -> output.set(x, y,
            (int) Math.sqrt(Math.pow(gx.get(x, y), 2.) + Math.pow(gy.get(x, y), 2.))));
  }

  public static void gradientImageSobel(GrayU8 input, GrayU8 output) {
    int[][] kernelX = { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
    int[][] kernelY = { { -1, -2, -1 }, { 0, 0, 0 }, { 1, 2, 1 } };
    gradientImage(input, output, kernelX, kernelY);
  }

  public static void gradientImagePrewitt(GrayU8 input, GrayU8 output) {
    int[][] kernelX = { { -1, 0, 1 }, { -1, 0, 1 }, { -1, 0, 1 } };
    int[][] kernelY = { { -1, -1, -1 }, { 0, 0, 0 }, { 1, 1, 1 } };
    gradientImage(input, output, kernelX, kernelY);
  }

  public static void gradient1D(GrayU8 input, GrayU8 output) {
    int[][] kernelX = { { -1, 0, 1 } };
    int[][] kernelY = { { -1 }, { 0 }, { 1 } };
    gradientImage(input, output, kernelX, kernelY);
  }

  public static void main(final String[] args) {
    // load image
    if (args.length < 2) {
      System.out.println("missing input or output image filename");
      System.exit(-1);
    }
    final String inputPath = args[0];
    GrayU8 input = UtilImageIO.loadImage(inputPath, GrayU8.class);
    GrayU8 output = input.createSameShape();

    // processing
    // meanFilter(input, output, 11);
    // gradientImageSobel(input, output);
    // gradientImagePrewitt(input, output);
    gradient1D(input, output);

    // save output image
    final String outputPath = args[1];
    UtilImageIO.saveImage(output, outputPath);
    System.out.println("Image saved in: " + outputPath);
  }

}

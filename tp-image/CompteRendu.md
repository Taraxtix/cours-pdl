# Gray Level

Toutes les fonctions utilisees dans cette partie ce trouve dans le fichier: GrayLevelProcessing.java

## Brighten

Input: images/vagues.jpg
![ThresholdInput](images/vagues.jpg)

`brighten(input, 50);`
Output: images/brighten_positive_output.png
![BrightenPositiveOutput](images/brighten_positive_output.jpg)

`brighten(input, -50);`
Output: images/brighten_negative_output.png
![BrightenNegativeOutput](images/brighten_negative_output.jpg)

## DynamicContrast
Input: images/image-1-low_dynamic.png
![DynamicInput](images/image-1-low_dynamic.png)

Output: images/low_dynamic_output.png
`dynamicContrast(input);`
![DynamicOutput](images/dynamic_contrast_output.png)

## HistogramEqualization
Input: images/street.png
![HistogramInput](images/street.png)

Output: images/histogram_equalization_output.png
`histogramEqualization(input);`
![HistogramOutput](images/histogram_equalization_output.png)

## Convolution
Toutes les fonctions utilisees dans cette partie ce trouve dans le fichier: GrayLevelProcessing.java

### MeanFilter
Input: images/histogram_equalization_output.png
![MeanInput](images/histogram_equalization_output.png)

Output: images/mean_filter_output.png
```java
meanFilter(input, output, 11);
```
![MeanOutput](images/mean_filter_output.png)

### Algorithme de convolution
#### Pourquoi le choix de GrayS16 ?
Le choix du type GrayS16 permet deux choses :
- On peut utiliser des valeurs negative pour coder les valeurs plus sombre (facilite la creation d'un filtre sous forme d'une matrice).
- On a deux fois plus de valeurs possibles pour des traitements plus precis sur les valeurs de gris.

### Gradient Sobel
Input: images/dynamic_contrast_output.png
![SobelInput](images/dynamic_contrast_output.png)

Output: images/gradient_sobel_output.png
```java
gradientImageSobel(input, output);
```
![SobelOutput](images/gradient_sobel_output.png)

### Gradient Prewitt
Input: images/dynamic_contrast_output.png
![PrewittInput](images/dynamic_contrast_output.png)

Output: images/gradient_prewitt_output.png
```java
gradientImagePrewitt(input, output);
```
![PrewittOutput](images/gradient_prewitt_output.png)

#### Comparaison entre les deux gradients
Le gradient de Prewitt produit une image plus sombre avec moins de bruits mais conserve moins les details (Hors bordures) que le gradient de Sobel.

### Gradient with 1D kernel
Input: images/dynamic_contrast_output.png
![PrewittInput](images/dynamic_contrast_output.png)

Output: images/gradient_1d_output.png
```java
gradient1D(input, output);
```
![PrewittOutput](images/gradient_1d_output.png)

# Color
Toutes les fonctions de cette partie ce trouve dans le fichier: ColorProcessing.java 

## Brighten
Input: images/paysage681x454.jpg
![BrightenInput](colorimages/paysage681x454.jpg)

Output: colorimages/brighten_output.jpg
```java
brighten(image, 50);
```
![BrightenOutput](colorimages/brighten_output.jpg)

## Mean Filter
Input: colorimages/hamac-1920x1280.jpg
![MeanFilterInput](colorimages/hamac-1920x1280.jpg)

Output: colorimages/mean_filter_output.jpg
```java
Planar<GrayU8> output = image.createSameShape();
meanFilter(image, output, 11);
```
NB:
Remplacer la ligne: `UtilImageIO.saveImage(image, outputPath);`
par: `UtilImageIO.saveImage(output, outputPath);`
![MeanFilterOutput](colorimages/mean_filter_output.jpg)

## Shade Gray
Input: colorimages/mountains.jpg
![ShadeGrayInput](colorimages/mountains.jpg)

Output: colorimages/shade_gray_output.jpg
```java
shadeGray(image);
```
![ShadeGrayOutput](colorimages/shade_gray_output.jpg)

## Conversion RGB/HSV

### Quels sont les intervalles de valeur obtenus pour les canaux H, S et V ? Que donne la conversion en HSV de la couleur d'un pixel gris (R=G=B)?

Les intervalles de valeur sont:
- H : 0 - 360
- S : 0 - 1
- V : le meme intervalle que le pixel RGB (Depend donc du type utiliser, 0 - 255 dans le cas de `Planar<GrayU8>`)

Lors de la conversion d'un pixel gris tel que `R = G = B = Value` alors la valeur HSV est de:
- H : 0
- S : 0
- V : Value

### Tint
Input: colorimages/poppy.jpg
![TintInput](colorimages/poppy.jpg)

Output: colorimages/tint_output.jpg
```java
tint(image, 270);
```
![TintOutput](colorimages/tint_output.jpg)

### Shade Gray HSV

Mettre toutes les valeurs de saturation d'une image a 0 fourni une image en noir et blanc.
Cela peut etre considere comme equivalant au filtre `shadeGray` mais n'est pas exactement le meme (Celui ci fourni une image plus claire).

Input: colorimages/poppy.jpg
![ShadeGrayHSVInput](colorimages/mountains.jpg)

Output: colorimages/shade_gray_hsv_output.jpg
```java
tint(image, 270);
```
![ShadeGrayHSVOutput](colorimages/shade_gray_hsv_output.jpg)

### Hue Histogram

Input: colorimages/maldives.jpg
![HueHistoInput](colorimages/maldives.jpg)

Output: colorimages/hue_histo_output.jpg
```java
GrayU8 output = new GrayU8(360, 200);
hueHisto(image, output);
```
NB:
Remplacer la ligne: `UtilImageIO.saveImage(image, outputPath);`
par: `UtilImageIO.saveImage(output, outputPath);`
![HueHistoOutput](colorimages/hue_histo_output.jpg)

### Hue Histogram

Input: colorimages/hamac-1920x1280.jpg
![HueHistoInput](colorimages/hamac-1920x1280.jpg)

Output: colorimages/hue_saturation_histo_output.jpg
```java
GrayU8 output = new GrayU8(360, 101);
hueSaturationHisto(image, output);
```
NB:
Remplacer la ligne: `UtilImageIO.saveImage(image, outputPath);`
par: `UtilImageIO.saveImage(output, outputPath);`

![HueHistoOutput](colorimages/hue_saturation_histo_output.jpg)
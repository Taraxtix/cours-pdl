import axios, { AxiosResponse } from "axios";

async function downloadImageById(id: number) {
    var imageUrl = "/images/" + id;
    var imageDataUrl: string = "";

    await axios.get(imageUrl, { responseType: "blob" })
        .then(function (response: AxiosResponse) {
            const reader = new window.FileReader();
            reader.readAsDataURL(response.data);
            reader.onload = function () {
                imageDataUrl = (reader.result as string);
            }
        }).catch(function (error) {
            console.log(error)
        });
    while (imageDataUrl == "") {
        await new Promise(resolve => setTimeout(resolve, 100));
    }
    return imageDataUrl;
}

async function getImagesInfo(): Promise<Array<{ name: string, id: number }>> {
    var imagesInfo: Array<{ name: string, id: number }> = [];

    await axios.get('/images').then(function (response) {
        imagesInfo = response.data
    }).catch(function (error) {
        console.log(error)
    })

    return imagesInfo;
}

function postImage(image: File) {
    const formData = new FormData();
    formData.append("file", image);
    axios.post('/images', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then(function (response) {
        console.log("sucessfully uploaded image: " + response.data);

    }).catch(function (error) {
        console.log(error)
    });
}

export { downloadImageById, getImagesInfo, postImage };
# Gallerie d'image Serveur/Client

## Serveur
Toute les fonctionnalite du serveur on ete implementer et tester.
Je n'ai pas rencontre de difficultes particuliere sur cette partie.

## Client
Toute les fonctionnalite du client on ete implementer et tester.
Cependant l'aspect esthetique reste minimaliste (bien que fonctionnel).
J'ai rencontree de legere difficules avec certains fonctions `async`, en effet j'ai mis du temps a comprendre comment bien synchroniser l'execution de ces fonctions afin que les elements que je voulais soient correctement affiche a l'ecran.
